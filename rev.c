#include <unistd.h>

int main() {
	char buf[1024];
	char *eob = buf + sizeof(buf) - 1;
	char *p = eob;
	while (read(STDIN_FILENO, p, 1) == 1 && --p >= buf);
	write(STDOUT_FILENO, p + 1, eob - p);
	return 0;
}
