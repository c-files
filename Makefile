XCBV_CFLAGS += $(shell pkg-config --cflags xcb-image xpm) -Wall -g $(CFLAGS)
XCBV_LDFLAGS += $(shell pkg-config --libs xcb-image xpm) -lrt

RGB_TXT := /usr/share/vim/vim73/rgb.txt

timeadd: timeadd.c
	$(CC) -g -Wall -Werror -Wextra $(CFLAGS) -o $(OUTDIR)$@ $< -lrt

xcbviewfs: xcbviewfs.c colorlookup.c
	$(CC) $(XCBV_CFLAGS) -o $@ $< colorlookup.c $(XCBV_LDFLAGS)

%: %.c
	$(CC) -g -Wall -Werror -Wextra $(CFLAGS) -o $(OUTDIR)$@ $<

colorlookup.c: $(RGB_TXT) colorlookupgen
	./colorlookupgen $(RGB_TXT) > $@

.PHONY: clean
clean:
	rm -vf xcbviewfs colorlookupgen colorlookup.c
