#include <stdio.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <malloc.h>

#include <error.h>
#include <errno.h>
#define fail(msg) error(1, errno, msg)

#ifndef PATH_MAX
#	define PATH_MAX 4096
#endif

struct ev {
	int val;
	char *name;
};

#define STRVAL(val) #val
#define IN(name) {IN_##name, STRVAL(name)}

struct ev events[] = {
	IN(CLOSE_WRITE),
	IN(CLOSE_NOWRITE),
	IN(OPEN),
	{0, NULL}
};

int main(int argc, char **argv) {
	int i, ifd, wfd, mask;
	int wfds[argc - 1];
	if (argc < 2) {
		fprintf(stderr, "Usage: %s file_to_watch ...\n", argv[0]);
		return 1;
	}
	for (i=1; i<argc; i++) {
		printf("file = %s\n", argv[i]);
	}

	ifd = inotify_init();
	if (ifd == -1) fail("inotify_init");
	printf("ifd = %i\n", ifd);

	mask = 0;

	struct ev *event = events;
	while (event->name != NULL) {
		mask |= event->val;
		printf("event IN_%-16s = %8x\n", event->name, event->val);
		++event;
	}
	for (i=1; i<argc; i++) {
		wfd = inotify_add_watch(ifd, argv[i], mask);
		if (wfd == -1) fail("inotify_add_watch");
		printf("wfd = %i\n", wfd);
		wfds[i - 1] = wfd;
	}

	int iev_size = sizeof(struct inotify_event) + PATH_MAX + 1;
	struct inotify_event *iev = malloc(iev_size);
	if (!iev) fail("malloc");

	putchar('\n');

	int j = 100;
	while (--j) {
		char *file;
		if (read(ifd, iev, iev_size) == -1) {
			perror("read");
			break;
		}
		for (i=0; i<argc-1; i++) {
			if (wfds[i] == iev->wd) {
				file = argv[i + 1];
				break;
			}
		}
		printf("wd = %i, mask = %x, name = %s\tfile = %s\n",
			iev->wd, iev->mask, iev->name,
			file);
	}

	free(iev);

	for (i=0; i<argc-1; i++) {
		wfd = wfds[i];
		if (inotify_rm_watch(ifd, wfd) == -1) fail("inotify_rm_watch");
	}

	close(ifd);
	return 0;
}
