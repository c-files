#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <fcntl.h>

#define F(cmd) ( (r=cmd),r<0?(perror(#cmd),exit(1),r):r )

int main() {
	int r;
	int pA[2];
	int f = F(open("mehlog", O_CREAT | O_APPEND | O_WRONLY, 0644));
	pipe(pA);
	//pipe(pB);
	if (!F(fork())) {
		close(f);
		dup2(pA[1], STDOUT_FILENO);
		close(pA[0]);
		close(pA[1]);
		F(execlp("cat", "cat", NULL));
	}
	close(pA[1]);
	if (!F(fork())) {
		dup2(f, STDOUT_FILENO);
		dup2(pA[0], STDIN_FILENO);
		close(pA[0]);
		close(f);
		F(execlp("rev", "rev", NULL));
	}
	close(pA[0]);
	close(f);
	wait(NULL);
	return 0;
}
