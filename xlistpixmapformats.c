#include <X11/Xlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
	Display *dpy = XOpenDisplay(NULL);
	int total = 0;
	XPixmapFormatValues *fs = XListPixmapFormats(dpy, &total);
	if (total) {
		int i;
		for (i=0; i<total; ++i) {
			XPixmapFormatValues f = fs[i];
			printf("depth=%2d bpp=%2d scanline_pad=%d\n",
				f.depth, f.bits_per_pixel, f.scanline_pad);
		}
	}
	XCloseDisplay(dpy);
	return 0;
}
