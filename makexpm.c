#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* chars to use for colors */
static const char p[] = "0123456789"
"abcdefghijklmnopqrstuvwxyz"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"!@#$%^&*()_+-=`~[]{}|;:',<.>/? ";
/* number of chars excl NIL */
static const int q = sizeof(p) - 1;

int main(int argc, char **argv) {
	unsigned int width, height, ncolors, cpp, i, j;
	if (argc != 5) {
		fprintf(stderr, "Usage: %s width height ncolors cpp\n", argv[0]);
		return 1;
	}
	/*
	width = 1600;
	height = 900;
	ncolors = width * height / 1024;
	cpp = 3;
	*/

	width = strtoul(argv[1], NULL, 0);
	height= strtoul(argv[2], NULL, 0);
	ncolors=strtoul(argv[3], NULL, 0);
	cpp   = strtoul(argv[4], NULL, 0);

	printf(
		"/* XPM */\n"
		"static char *picture[] = {\n"
		"/* columns rows colors chars-per-pixel */\n"
		"\"%d %d %d %d \",\n"
		, width, height, ncolors, cpp);


	const char *fmt;
	switch (cpp) {
	case 1: fmt = "%c"; break;
	case 2: fmt = "%c%c"; break;
	case 3: fmt = "%c%c%c"; break;
	default:fmt = "%c%c%c%c"; break;
	}
	for (i = 0; i<ncolors; ++i) {
		putchar('"');
		printf(fmt, p[i%q], p[i/q%q], p[i/q/q%q], p[i/q/q/q%q]);
		printf(" c #%06x\",\n", 256 * 256 * 256 * i / ncolors);
	}

	puts("/* pixels */");
	for (i = 0; i<height; ++i) {
		putchar('"');
		for (j = 0; j<width; ++j) {
			unsigned int n = (i * width + j) % ncolors;
			printf(fmt, p[n%q], p[n/q%q], p[n/q/q%q], p[n/q/q/q%q]);
		}
		if (i < height - 1) puts("\",");
		else puts("\"");
	}
	puts("};");
	return 0;
}
