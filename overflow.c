/**
 * 27-08-2012 buffer overflow exercise
 * gcc -fstack-protector-all -D_FORTIFY_SOURCE=2 -O2 -Wall -g overflow.c -o overflow
 */
#include <stdio.h>
#include <stdlib.h>

void g() {
	static int j = 0;
	printf("Call %u\n", ++j);
}
void bye() {
	puts("Last call - ciao");
	exit(42);
}

void f() {
	unsigned int i;
	void * x[0];
	scanf("%u", &i);
	puts("From f()");
	x[++i] = bye;
	while (i--)
		x[i] = g;
	printf("Stuff: %p\n", x);
}

int main() {
	f();
	return 0;
}
