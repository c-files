/**
 * Attempt to display picture fullscreen, capturing input (failed at capture kbd keys)
 * Author: Peter Wu
 * Date: 2012-09-24
 */
#include <QApplication>
#include <QLabel>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

#define WITH_XGRAB
//#undef WITH_XGRAB
#ifdef WITH_XGRAB
#	include <X11/Xlib.h>
#endif

class Application: public QApplication {
public:
	Application(int argc, char **argv): QApplication(argc, argv) {}
	~Application(){}
	bool notify(QObject *object, QEvent *event) {
		std::cerr << "Event: " << event->type() << std::endl;
		return object->event(event);
	}
};

int main(int argc, char **argv) {
	Application app(argc, argv);
	QLabel lbl;
        //lbl.setCursor(QCursor(Qt::BlankCursor));
	lbl.resize(300, 200);
	lbl.setWindowTitle(*argv);
	lbl.show();
	lbl.grabKeyboard();
#ifdef WITH_XGRAB
	Display *dpy = XOpenDisplay(NULL);
	if (!dpy) fprintf(stderr, "Could not open DISPLAY\n");
	else{
		Window win = DefaultRootWindow(dpy);
		XGrabKeyboard(dpy, win, False,
			GrabModeAsync,
			GrabModeAsync,
			CurrentTime);
#if 0
		XGrabPointer(dpy, win, True,
			0,
			GrabModeAsync,
			GrabModeAsync,
			win,
			None,
			CurrentTime);
#endif
	}
#endif
	return app.exec();
	//std::cerr << "Closing" << std::endl;
	//return 0;
}
