#include <QtGui>
#include <iostream>
#include <time.h>

int keys[] = {
	Qt::Key_Z, /* prev wep */
	Qt::Key_X, /* next wep */
	Qt::Key_Space, /* shoot */
	Qt::Key_Up,
	Qt::Key_Left,
	Qt::Key_Down,
	Qt::Key_Right,
	Qt::Key_P,
	Qt::Key_Backspace,
};

std::string key_names[] = {
	"Z",
	"X",
	"Space",
	"Up",
	"Left",
	"Down",
	"Right",
	"Pause",
	"Backspace",
};

#define ARRAY_SIZE(a) (sizeof (a) / sizeof *(a) )

class Application: public QApplication {
public:
	Application(int & argc, char **argv): QApplication(argc, argv) {
		setbuf(stdout, NULL); /* disable buffering */
	}
	~Application(){}
	bool notify(QObject *object, QEvent *event) {
		bool is_pressed = event->type() == QEvent::KeyPress;
		if (is_pressed || event->type() == QEvent::KeyRelease) {
			if (handle_key(static_cast<QKeyEvent *>(event)->key(), is_pressed))
				return true;
		}
		return object->event(event);
	}
private:
	bool handle_key(int code, bool is_pressed) {
		for (unsigned int i=0; i<ARRAY_SIZE(keys); ++i) {
			if (keys[i] == code) {
				report_key(i + 1, is_pressed);
				return true;
			}
		}
		if (code == Qt::Key_Shift) {
			release_key();
			return true;
		}
		if (code == Qt::Key_Escape) {
			std::cerr << "Escape received - quitting" << std::endl;
			quit();
		}
		return false;
	}
	int getMS() {
		struct timespec ts;
		if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
			perror("clock_gettime");
			return 0;
		}
		return (ts.tv_sec * 1000) + ts.tv_nsec / 1000000;
	}
	int last_code;
	bool last_is_pressed;
	//int last_time;
	void release_key() {
		if (last_code && last_is_pressed) {
			send_key(last_code, false);
			last_is_pressed = false;
		}
	}
	bool always_send_key(int key) {
		if (!key) return false;
		switch (keys[key - 1]) {
		case Qt::Key_Up:
		case Qt::Key_Down:
		case Qt::Key_Left:
		case Qt::Key_Right:
			return false;
		}
		return true;
	}
	void report_key(int key, bool is_pressed) {
		//int timestamp = getMS();
		//bool timed_out = timestamp - last_time >= 300;
		if (last_code == key && !always_send_key(key)) {
			if ((is_pressed == last_is_pressed) || 
				(last_is_pressed && !is_pressed)) {
				//if (!timed_out) {
					return;
				//}
			}
		} else {
			if (!always_send_key(last_code))
				release_key();
		}
		last_code = key;
		last_is_pressed = is_pressed;
		//last_time = timestamp;

		send_key(key, is_pressed);
	}
	void send_key(int key, bool is_pressed) {
		printf("%u %u\n", key, is_pressed);
		if (is_pressed)
			std::cerr << key_names[key - 1] << " " << (is_pressed ? "pressed" : "released") << std::endl;
	}
};


int main(int argc, char **argv) {
	Application app(argc, argv);

	QWidget window;
	window.resize(250, 150);
	window.setWindowTitle("Remote keyboard");
	window.show();

	return app.exec();
}
