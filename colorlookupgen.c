#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv) {
	FILE *file = stdin;
	char *funcname = "color_lookup";
	char line[256];
	char color[256];
	unsigned int total_colors = 0;

	if (argc >= 2) {
		file = fopen(argv[1], "r");
		if (!file) {
			perror("Error opening rgb.txt");
			return 1;
		}
	}

	if (argc >= 3) {
		funcname = argv[1];
	}

	printf("#include <string.h>\n");
	printf("int %s(char *name, unsigned char color[3]) {\n", funcname);
	printf(
		"  static struct {\n"
		"    char *name;\n"
		"    unsigned char color[3];\n"
		"  } colors[] = {\n");
	while (fgets(line, sizeof(line), file) != NULL) {
		uint8_t r, g, b;
		int matches = sscanf(line, "%hhd %hhd %hhd %255[^\n]",
			&r, &g, &b, color);
		if (matches == 4) {
			++total_colors;
			printf("    { \"%s\", { %d, %d, %d } },\n", color, r, g, b);
		}
	}
	printf(
		"  };\n"
		"  int i = 0;\n"
		"  for (; i<%d; ++i)\n"
		"    if (!strcasecmp(colors[i].name, name)) {\n"
		"      memcpy(color, colors[i].color, 3);\n"
		"      return 1;\n"
		"    }\n"
		"  return 0;\n"
		"}\n", total_colors);

	return 0;
}
