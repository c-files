#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include "iwlib.h"
//#include "wireless.22.h"
//make wireless.h

int main() {
	int r;
	int skfd;
	int request;
	struct iwreq wrq;

	skfd = iw_sockets_open();
	if (skfd == -1) {
		fprintf(stderr, "skfd fail\n");
		return 1;
	}
	request = SIOCGIWAP;

	memset(&wrq, 0, sizeof(wrq));
	strcpy(wrq.ifr_name, "wlan0");

	r = ioctl(skfd, request, &wrq);
	printf("ret = %x\n", r);
	struct sockaddr sa = wrq.u.ap_addr;
	char *data = sa.sa_data;
	int i;
	for (i=0; i<14; i++)
		printf("%02X ", data[i]);
	printf("\n");
	return 0;
}
