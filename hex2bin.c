#include <stdint.h>
#include <stdio.h>
#include <ctype.h>

int main() {
	char c;
	int n = 0;
	int count = 0;
	int skipLine = 0;
	while ((c = getchar()) != EOF) {
		if (skipLine && c != '\n') continue;
		if (isxdigit(c)) {
			++count;
			n <<= 4;
			if (isdigit(c)) {
				n |= c - '0';
			} else {
				n += 10 + tolower(c) - 'a';
			}
		}
		if (count >= 2 || (count && !isxdigit(c))) {
			putchar(n);
			n = count = 0;
		}
		skipLine = c == '#';
	}
	return 0;
}
