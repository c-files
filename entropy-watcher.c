/**
 * Watch available kernel entropy. By default, it prints the available entropy
 * and difference to the previously recorded entropy every second, but this can
 * be changed with arg1 (in milliseconds). You can also press Enter to
 * immediately print the current entropy.
 *
 * Author: Peter Wu <lekensteyn@gmail.com>
 * Date: 2013-01-28
 */
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <poll.h>
#include <stdlib.h>
#include <stdbool.h>

#define FILENAME "/proc/sys/kernel/random/entropy_avail"

int main(int argc, char **argv) {
	char buff[32];
	ssize_t r;
	struct pollfd pfd;
	int fd;
	int print_interval_ms;
	long int avail, prev = 0;

	print_interval_ms = 1000;
	if (argc > 1) {
		print_interval_ms = atoi(argv[1]);
	}

	fd = open(FILENAME, O_RDONLY);
	if (fd < 0) {
		perror(FILENAME);
		return 1;
	}

	pfd.fd = STDIN_FILENO;
	pfd.events = POLLIN;

	while ((r = read(fd, buff, sizeof(buff))) > 0) {
		/* remove possible newline and terminate string for sure */
		buff[r - 1] = 0;
		avail = strtol(buff, NULL, 10);
		printf("%li (%+li)", avail, avail - prev);
		fflush(stdout);
		prev = avail;

		poll(&pfd, 1, print_interval_ms);
		lseek(fd, 0, SEEK_SET);
		if (pfd.revents == POLLIN) { /* pressed enter? */
			r = read(STDIN_FILENO, buff, sizeof(buff));
			if (r > 0 && buff[r - 1] == '\n')
				continue;
		}
		putchar('\n');
	}

	close(fd);

	return 0;
}
