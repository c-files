/**
 * Shows picture arg1 fullscreen
 * Author: Peter Wu
 * Date: 2012-09-22
 */
#include <QApplication>
#include <QLabel>
#include <iostream>

int main (int argc, char **argv) {
	QApplication app(argc, argv);
	QStringList args(app.arguments());
	if (args.size() < 2) {
		std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
		return 1;
	}

	QString filename(args[1]);
	QPixmap pic(filename);
	if (!pic) {
		std::cerr << "Invalid image: " << filename.toStdString() << std::endl;
		return 1;
	}

	QLabel lbl;
	lbl.setCursor(QCursor(Qt::BlankCursor));
	lbl.setPixmap(pic);
	lbl.setScaledContents(true);
	lbl.showFullScreen();
	return app.exec();
}
