#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

int main(int argc, char **argv) {
	int i;
	for (i=1; i<argc; i++) {
		uint32_t n = strtoul(argv[i], NULL, 16);
		write(STDOUT_FILENO, &n, 4);
	}
	return 0;
}
